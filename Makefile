
PREFIX=$(HOME)/.local
PREFIX_BIN=$(PREFIX)/bin

BINARY_PATH=$(PREFIX_BIN)/szcalc

SUBMODULES = deps/euphrates/src deps/czempak/src

CZEMPAK = CZEMPAK_ROOT=$(PWD)/.czempak-root ./dist/czempak

all: build test1 install

test1: dist/czempak
	cat test/input.elc | $(CZEMPAK) run test/test.scm

test2: dist/szcalc
	cat test/input.elc | dist/szcalc --file test/input.elc

build: dist/szcalc

install: $(BINARY_PATH)

$(BINARY_PATH): dist/szcalc $(PREFIX_BIN)
	cp $(PWD)/dist/szcalc $(PREFIX_BIN)

$(PREFIX_BIN):
	mkdir -p "$@"

reinstall: | clean uninstall install

uninstall:
	rm -f $(PREFIXBIN)/szcalc

dist/szcalc: src/*.scm dist dist/czempak $(SUBMODULES)
	$(CZEMPAK) install src/main.scm "$@"

dist/czempak: $(SUBMODULES)
	cd deps/czempak && $(MAKE) PREFIXBIN=$(PWD)/dist

deps/czempak/src:
	git submodule update --init

deps/euphrates/src:
	git submodule update --init

clean:
	git submodule foreach --recursive 'git clean -dfx'
	git clean -dfx

dist:
	mkdir -p "$@"

.PHONY: all build install reinstall clean test1 test2 test3

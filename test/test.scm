;;;; Copyright (C) 2020, 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (read-tree) "./../src/read-tree.scm"
%use (debug) "./../src/euphrates/debug.scm"
%use (current-out/p loop-eval default-eval-tree default-apply-branch) "./../src/eval.scm"
%use (let-rule) "./../src/let-rule.scm"
%use (eq-rule) "./../src/eq-rule.scm"
%use (add-rule mul-rule exp-rule modulo-rule base-rule binor-rule binand-rule binxor-rule bincount-rule binneg-rule binsneg-rule floor-rule ceiling-rule)  "./../src/math-rules.scm"
%use (cc-rule continue) "./../src/cc-rule.scm"
%use (redsl-rule redsl-read-rule redsl-parse redsl-parse-rule) "./../src/redsl.scm"
%use (less-rule) "./../src/less-rule.scm"
%use (read-lines-rule) "./../src/read-lines-rule.scm"
%use (alpha-beta-reduce-rule free-variables-rule) "./../src/lambda-calculus.scm"
%use (virtual-env/p virtual-env-create-from-alist) "./../src/virtual-env.scm"
%use (ref-rule) "./../src/ref-rule.scm"
%use (atom-rule) "./../src/atom-rule.scm"
%use (negation-rule) "./../src/negation-rule.scm"
%use (fresh-rule) "./../src/fresh-rule.scm"
%use (galphaunique-rule) "./../src/alphaunique.scm"
%use (show-tree) "./../src/show-tree.scm"

(use-modules (ice-9 pretty-print))

(define input-tree (read-tree))

(define (test-out T)
  (when T (show-tree T))
  T)

(define T1
  (parameterize ((current-out/p test-out)
                 (virtual-env/p
                  (virtual-env-create-from-alist
                   (list
                    (cons '$lt less-rule)
                    (cons '= eq-rule)
                    (cons '$add add-rule)
                    (cons '$mul mul-rule)
                    (cons '$exp exp-rule)
                    (cons '$mod modulo-rule)
                    (cons '$base base-rule)
                    (cons '$binor binor-rule)
                    (cons '$binand binand-rule)
                    (cons '$binxor binxor-rule)
                    (cons '$binneg binneg-rule)
                    (cons '$binsneg binsneg-rule)
                    (cons '$bincount bincount-rule)
                    (cons '$floor floor-rule)
                    (cons '$ceiling ceiling-rule)
                    (cons '$ref ref-rule)
                    (cons '$atom atom-rule)
                    (cons '$FV free-variables-rule)
                    (cons '$not negation-rule)
                    (cons '$fresh fresh-rule)
                    (cons '$galphaunique galphaunique-rule)
                    ))))

    (loop-eval
     (list
      alpha-beta-reduce-rule
      (let-rule '$let)
      (cc-rule '$cc)
      (redsl-rule 'redsl)
      (redsl-read-rule 'redsl-read)
      (redsl-parse-rule 'redsl-parse)
      )
     default-eval-tree default-apply-branch input-tree)))

(newline)
(newline)
(pretty-print T1)
;; (debug "CONT: ~s" (continue T1 777))

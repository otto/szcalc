;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%var make-fresh-generator
%var global-fresh-generator

(define fresh-generator-dot-pool
  #(.a .b .c .d .e .f .g .h .i .j .k .l .m .n .o .p .q .r .s .t .u .v .w .x .y .z))
(define fresh-generator-comma-pool
  #(,a ,b ,c ,d ,e ,f ,g ,h ,i ,j ,k ,l ,m ,n ,o ,p ,q ,r ,s ,t ,u ,v ,w ,x ,y ,z))

(define make-fresh-generator-private
  (lambda (pool)
    (let* ((len (vector-length pool))
           (counter 0)
           (counter2 1))
      (lambda _
        (let* ((y (vector-ref pool counter))
               (m (if (> counter2 1)
                      (string->symbol
                       (string-append
                        (symbol->string y)
                        (number->string counter2)))
                      y)))
          (set! counter (+ 1 counter))
          (when (>= counter len)
            (set! counter 0)
            (set! counter2 (+ 1 counter2)))
          m)))))

(define (make-fresh-generator)
  (make-fresh-generator-private fresh-generator-comma-pool))

(define global-fresh-generator
  (make-fresh-generator-private fresh-generator-dot-pool))

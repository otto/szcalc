;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (hashmap) "./euphrates/hashmap.scm"
%use (hashmap-ref hashmap-set! hashmap-clear! hashmap->alist) "./euphrates/ihashmap.scm"
%use (raisu) "./euphrates/raisu.scm"
%use (simple-predicate) "./simple-predicate.scm"
%use (prepare-match replace-match) "./match.scm"
%use (global-fresh-generator) "./fresh.scm"

%var galphaunique-rule

(define galphaunique-rule
  (simple-predicate
   (body result declaration usage scope-pattern)
   (set-body set-result set-declaration set-usage set-scope-pattern)

   (define declare-pattern (car declaration))
   (define declare-proj (cadr declaration))
   (define use-pattern (car usage))
   (define use-proj (cadr usage))

   (define dp (prepare-match declare-pattern))
   (define up (prepare-match use-pattern))
   (define sp (prepare-match scope-pattern))

   (define H (hashmap))

   (define stack (list (hashmap)))
   (define (lookup name)
     (let loop ((stack stack))
       (if (null? stack) #f
           (let ((peek (car stack)))
             (or (hashmap-ref peek name #f)
                 (loop (cdr stack)))))))
   (define (add name)
     (hashmap-set! (car stack) name (global-fresh-generator)))

   (set-result
    (let loop ((term body))

      (cond

       ((begin (hashmap-clear! H)
               (null? (dp H term)))

        (let* ((arg (hashmap-ref H declare-proj #f)))
          (unless arg (raisu 'declare-projection-not-found term declare-pattern declare-proj))
          (add arg)
          (map loop term)))

       ((begin (hashmap-clear! H)
               (null? (up H term)))
        (let* ((arg (hashmap-ref H use-proj #f)))
          (unless arg (raisu 'use-projection-not-found term use-pattern use-proj))
          (let* ((alpha (lookup arg)))
            (if (not alpha) term ;; not declared
                (begin
                  (hashmap-set! H use-proj alpha)
                  (replace-match H use-pattern))))))

       ((begin (hashmap-clear! H)
               (null? (sp H term)))
        (set! stack (cons (hashmap) stack))
        (let ((R (map loop term)))
          (set! stack (cdr stack))
          R))

       ((pair? term)
        (map loop term))

       (else term))))

   #t))


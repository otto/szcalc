;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (hashmap-set! hashmap-ref hashmap->alist) "./euphrates/ihashmap.scm"
%use (args->values) "./virtual-env.scm"

%var simple-predicate

(define-syntax simple-predicate-helper
  (syntax-rules ()
    ((_ argv H args0 (a . args) args-buf (s . set-args) set-args-buf bodies)
     (simple-predicate-helper
      argv H args0 args (a . args-buf) set-args
      ((s (lambda (value) (hashmap-set! H a value) ))  . set-args-buf)
      bodies))
    ((_ argv H args0 () args-buf () set-args-buf bodies)
     (apply
      (lambda args0
        (let set-args-buf
            (apply
             (lambda args0 . bodies)
             (args->values H argv))))
      argv))))

(define-syntax simple-predicate
  (syntax-rules (_)
    ((simple-predicate args _ . bodies)
     (lambda (venv env eval-tree apply-branch H key argv)
       (apply (lambda args . bodies)
              (args->values H argv))))
    ((simple-predicate args set-args . bodies)
     (lambda (venv env eval-tree apply-branch H key argv)
       (simple-predicate-helper argv H args args () set-args () bodies)))))


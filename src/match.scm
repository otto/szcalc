;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (hashmap hashmap?) "./euphrates/hashmap.scm"
%use (hashmap->alist hashmap-ref hashmap-set! hashmap-clear! hashmap-foreach hashmap-copy) "./euphrates/ihashmap.scm"
%use (curry-if) "./euphrates/curry-if.scm"
%use (variable-name?) "./variable-name-p.scm"

%var prepare-match
%var replace-match

(use-modules (srfi srfi-9)) ;; records

(define-record-type <reversed-list>
  (reversed-list value) reversed-list?
  (value reversed-list-value set-reversed-list-value!))

(define (list-singleton? L)
  (and (pair? L)
       (null? (cdr L))))

(define (hashmap-set-all! hash other-hash)
  (hashmap-foreach
   (lambda (key value)
     (hashmap-set! hash key value))
   other-hash))

(define (hashmap-set-equal-to! hash other-hash)
  (hashmap-clear! hash)
  (hashmap-set-all! hash other-hash))

(define (replace-match hash result)
  (define (repl cur)
    (cond
     ((list? cur)
      (if (kleene-star? cur)
          (let ((get (hashmap-ref hash (cadr cur) #f)))
            (if get
                (if (list? get)
                    get
                    (list get))
                (list cur)))
          (list (apply append (map repl cur)))))
     (else
      (list (hashmap-ref hash cur cur)))))

  (define (loop result)
    (if (null? result) '()
        (let ((cur (car result)))
          (cond
           ((list? cur)
            (if (kleene-star? cur)
                (let ((get (hashmap-ref hash (cadr cur) #f)))
                  (if get
                      (if (list? get)
                          (append get (loop (cdr result)))
                          (cons get (loop (cdr result))))
                      (cons cur (loop (cdr result)))))
                (cons (apply append (map repl cur))
                      (loop (cdr result)))))
           (else
            (let* ((new0 (hashmap-ref hash cur cur))
                   (new ((curry-if list-singleton? car) new0)))
              (cons new
                    (loop (cdr result)))))))))

  (cond
   ((kleene-star? result)
    (hashmap-ref hash (cadr result) result))
   ((pair? result)
    (loop result))
   (else
    (hashmap-ref hash result result))))

(define (kleene-star? x)
  (and (pair? x) (equal? '@ (car x))))

(define (list-split/reversed n lst)
  (let loop ((lst lst) (n n) (buf '()))
    (if (= n 0)
        (cons buf lst)
        (loop (cdr lst) (- n 1) (cons (car lst) buf)))))

(define (match-kleene-star/single star-count hash pattern0 rest buf)
  (let* ((rest-len (length rest))
         (buf-len (length buf))
         (var (cadr pattern0))
         (c-1 (- star-count 1)))

    (if (< buf-len rest-len) #f
        (let* ((S (list-split/reversed
                   (- buf-len rest-len) buf))
               (taken (car S))
               (left (cdr S)))
          (if (variable-name? var)
              (let ((current (hashmap-ref hash var #f))
                    (new (reversed-list taken)))
                (if current
                    (and (equal? current new)
                         (match* c-1 hash rest left))
                    (begin
                      (hashmap-set! hash var new)
                      (match* c-1 hash rest left))))
              (and (and-map (lambda (e) (equal? e var)) taken)
                   (match* c-1 hash rest left)))))))

(define (match-kleene-star/multiple star-count hash pattern0 rest buf)
  (let ((pattern (cdr pattern0))
        (H0 (hashmap-copy hash))
        (H (hashmap)))

    (for-each (lambda (p) (if (variable-name? p) (hashmap-set! H0 p (reversed-list '())) p)) pattern)

    (hashmap-set-equal-to! H H0)

    (if (null? (match* (- star-count 1) H rest buf))
        (begin
          (hashmap-set-all! hash H)
          (list))

        (let loop ((buf buf))
          (hashmap-set-equal-to! H H0)
          (let* ((M (match* (- star-count 1) H pattern buf)))
            (and M
                 (if (null? (match* (- star-count 1) H rest M))
                     (begin
                       (hashmap-set-all! hash H)
                       '())
                     (if (null? M) #f ;; TODO: is this ok?
                         (loop M)))))))))

(define (match-kleene-star star-count hash pattern0 rest buf)
  ;; (@ x) a b 1 (@ z) 0
  ;; 00000010 -> x = (0000) , a = 0 , b = 0, z = ()
  ;; 00010010 -> x = (0001) , a = 0 , b = 0, z = ()

  (and (list? buf)
       (if (<= star-count 0)
           (match-kleene-star/single star-count hash pattern0 rest buf)
           (match-kleene-star/multiple star-count hash pattern0 rest buf))))

(define (match-subtree star-count hash pattern buf)
  (and (pair? buf)
       (null? (match* star-count hash pattern (car buf)))
       (cdr buf)))

(define (match1 star-count hash pattern buf)
  (cond
   ((variable-name? pattern)
    (and (pair? buf)
         (let ((get (hashmap-ref hash pattern #f)))
           (if get
               (if (reversed-list? get)
                   (begin
                     (set-reversed-list-value! get (cons (car buf) (reversed-list-value get)))
                     (cdr buf))
                   (begin
                     (and (equal? get (car buf))
                          (cdr buf))))
               (begin
                 (hashmap-set! hash pattern (car buf))
                 (cdr buf))))))
   ((kleene-star? pattern)
    (match-kleene-star star-count hash pattern (list) buf))
   ((pair? pattern)
    (match-subtree star-count hash pattern buf))
   (else
    (and (pair? buf)
         (equal? pattern (car buf))
         (cdr buf)))))

(define (match* star-count hash pattern T)
  (let loop ((buf1 pattern) (buf2 T))
    (if (null? buf1) buf2
        (if (kleene-star? (car buf1))
            (match-kleene-star star-count hash (car buf1) (cdr buf1) buf2)
            (let ((r (match1 star-count hash (car buf1) buf2)))
              (and r (loop (cdr buf1) r)))))))

(define (get-star-count T)
  (let loop ((T T))
    (if (kleene-star? T)
        (+ 1 (apply + (map get-star-count (cdr T))))
        (if (pair? T)
            (apply + (map loop T))
            0))))

(define (remove-reversed-lists! H)
  (hashmap-foreach
   (lambda (key V)
     (hashmap-set!
      H key
      (if (reversed-list? V)
          (reverse (reversed-list-value V))
          V)))
   H))

(define (prepare-match pattern)
  (define star-count (get-star-count pattern))
  (lambda (H T)
    (let ((R (match* star-count H pattern T)))
      (remove-reversed-lists! H)
      R)))



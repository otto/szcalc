;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (rule-lambda) "./rule-lambda.scm"

%var functional-rule-lambda

(define-syntax functional-rule-lambda
  (syntax-rules ()
    ((_ (arg . args) . bodies)
     (let ((len (length (quote (arg . args)))))
       (rule-lambda
        (env eval-tree apply-branch T)
        (and (equal? (length T) len)
             (apply (lambda (arg . args) . bodies) T)))))
    ((_ args . bodies)
     (rule-lambda
      (env eval-tree apply-branch T)
      (apply (lambda args . bodies) T)))))


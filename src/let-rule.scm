;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (make-match-rule) "./make-match-rule.scm"
%use (loop-eval) "./eval.scm"
%use (rule-lambda) "./rule-lambda.scm"

%var let-rule

(define let:parse-rule
  (lambda (T)
    (and (not (null? T))
         (not (null? (cdr T)))
         (let* ((pattern (car T))
                (result (cadr T))
                (conditions (cddr T)))
           (make-match-rule pattern result conditions)))))

(define let-rule
  (rule-lambda
   (env eval-tree apply-branch args)
   (and (not (null? args))
        (not (null? (cdr args)))
        (null? (cddr args))
        (let* ((patterns (car args))
               (parsed (map let:parse-rule patterns)))
          (and (and-map identity parsed)
               (let ((env* (append parsed env))
                     (body (cadr args)))
                 (loop-eval env* eval-tree apply-branch body)))))))



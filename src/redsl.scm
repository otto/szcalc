;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (hashmap hashmap?) "./euphrates/hashmap.scm"
%use (hashmap-ref hashmap-set! hashmap-clear! hashmap-foreach hashmap-copy hashmap->alist alist->hashmap) "./euphrates/ihashmap.scm"
%use (read-list) "./euphrates/read-list.scm"
%use (read-string-file) "./euphrates/read-string-file.scm"
%use (list-fold) "./euphrates/list-fold.scm"
%use (string->lines) "./euphrates/string-to-lines.scm"
%use (string-trim-chars) "./euphrates/string-trim-chars.scm"
%use (list-tag/next) "./euphrates/list-tag-next.scm"
%use (comp) "./euphrates/comp.scm"
%use (list-break) "./euphrates/list-break.scm"
%use (string-null-or-whitespace?) "./euphrates/string-null-or-whitespace-p.scm"
%use (curry-if) "./euphrates/curry-if.scm"
%use (list-split-on) "./euphrates/list-split-on.scm"
%use (raisu) "./euphrates/raisu.scm"
%use (make-match-rule) "./make-match-rule.scm"
%use (loop-eval topdown-eval-tree) "./eval.scm"
%use (rule-lambda) "./rule-lambda.scm"
%use (functional-rule-lambda) "./functional-rule.scm"
%use (list-type) "./list-type.scm"

%var redsl-rules-type
%var strategy-type
%var redsl-rule
%var redsl-read
%var redsl-read-rule
%var redsl-parse
%var redsl-parse-rule

;; NOTE: rules are in reversed order
(define (group-lines lns)
  (define (string-label? line)
    (and (> (string-length line) 1)
         (string-suffix? ":" line)))
  (define (label? line)
    (symbol? line))

  (define (preprocessor line0)
    (let ((line (string-trim-chars line0 " \t" 'both)))
      (if (and (not (string-prefix? " " line0))
               (not (string-prefix? "\t" line0))
               (string-label? line))
          (string->symbol (substring line 0 (- (string-length line) 1)))
          line)))
  (define preprocessed
    (map preprocessor
         (filter (negate string-null-or-whitespace?) lns)))

  (car
   (list-fold
    (acc (cons (hashmap) #f))
    (x preprocessed)

    (let ((H (car acc))
          (t (cdr acc)))

      (if (label? x)
          (cons H x)
          (begin
            (hashmap-set! H t (cons x (hashmap-ref H t '())))
            (cons H t)))))))

(define (parse-single-line line)
  (define p (open-input-string line))
  (define bar (string->symbol "|"))
  (define bar? (comp (equal? bar)))
  (define arrow (string->symbol "->"))
  (define arrow? (comp (equal? arrow)))
  (define red (read-list p))

  (define-values (exp conds0) (list-break bar? red))
  (define conds
    (if (null? conds0) conds0
        (map cdr (list-tag/next #f bar? conds0))))

  (define-values (pattern result0) (list-break arrow? exp))

  (if (null? result0) #f
      (let ((result (cdr result0)))
        (cons pattern (cons result conds)))))

(define (parsed-line->rule p)
  (make-match-rule (car p) (cadr p) (cddr p)))

(define (line-groups->parsed-groups H)
  (define alist (hashmap->alist H))

  (define (line-group->rule-group p)
    (define key (car p))
    (define lines (cdr p))
    (define plines (filter identity (map parse-single-line lines)))
    (cons key plines))

  (map line-group->rule-group alist))

(define redsl-rules-type 'redsl-rules)
(define strategy-type 'strategy)

(define redsl-parse
  (compose (lambda (x) (cons redsl-rules-type x))
           line-groups->parsed-groups
           group-lines
           string->lines))

(define redsl-parse-rule
  (functional-rule-lambda
   (filepath)
   (redsl-parse filepath)))

(define redsl-read
  (compose redsl-parse read-string-file))

(define redsl-read-rule
  (functional-rule-lambda
   (filepath)
   (redsl-read filepath)))

(define (parsed-groups->rule-groups alist)
  (alist->hashmap
   (map
    (lambda (p)
      (define key (car p))
      (define plines (cdr p))
      (define rules (map parsed-line->rule plines))
      (define reversed-rules (reverse rules))
      (cons key reversed-rules))
    alist)))

(define (parse-re H re-tree0)
  (define re-tree
    (cdr re-tree0))

  (define (ref s)
    (let* ((ret (hashmap-ref H (string->symbol s) #f)))
      (unless ret
        (raisu 'Bad-regex s (hashmap->alist H)))
      ret))

  (define bar? (comp (equal? #\|)))

  (define (handle-splits s)
    (if (string-index s #\|)
        (let* ((names (map list->string (list-split-on bar? (string->list s)))))
          (apply append (map ref names)))
        (ref s)))

  (define (mapper elem)
    (let loop ((elem elem))
      (define (rec tag)
        (list (cons tag (apply append (map loop (cdr elem))))))
      (define (one tag s)
        (list (cons tag (handle-splits s))))

      (if (pair? elem)
          (case (car elem)
            ((* loop) (rec 'star))
            ((all) (rec 'all)) ;; evaluates all in series. The top-level default
            ((topdown) (rec 'topdown)) ;; changes eval-tree
            ((default) (rec 'default)) ;; changes eval-tree
            ((epsilon) (list)) ;; skip
            (else (raisu 'expected-*-or-loop-or-topdown-or-default elem)))
          (let ((s (symbol->string elem)))
            (if (string-suffix? "*" s)
                (let ((s (substring s 0 (- (string-length s) 1))))
                  (one 'star1 s))
                (one 'single s))))))

  (apply append (map mapper re-tree)))

(define (eval-re eval-tree0 apply-branch re T)
  (define eval1
    (lambda (eval-tree env T)
      (let ((T* (eval-tree env eval-tree apply-branch T)))
        (if T* (cons T* #t) (cons T #f)))))
  (define eval*
    (lambda (eval-tree env T) (loop-eval env eval-tree apply-branch T cons)))

  (car
   (let loop ((re re) (T T) (eval-tree eval-tree0))
     (define evaled? #f)
     (cons
      (list-fold
       (T T)
       (x re)

       (let* ((type (car x))
              (value (cdr x)))

         (define (rec R)
           (when (cdr R) (set! evaled? #t))
           (car R))

         (case type
           ((all)
            (rec (loop value T eval-tree)))
           ((topdown)
            (rec (loop value T topdown-eval-tree)))
           ((default)
            (rec (loop value T eval-tree0)))
           ((single)
            (rec (eval1 eval-tree value T)))
           ((star1)
            (rec (eval* eval-tree value T)))
           ((star)
            (let l2 ((T T))
              (let ((R (loop value T eval-tree)))
                (if (cdr R)
                    (begin
                      (set! evaled? #t)
                      (l2 (car R)))
                    (car R)))))
           (else
            (raisu 'unknown-type: type)))))

      evaled?))))

(define redsl-rule
  (rule-lambda
   (env eval-tree apply-branch args)
   (and (not (null? args))
        (not (null? (cdr args)))
        (not (null? (cddr args)))
        (null? (cdddr args))
        (pair? (car args))
        (equal? redsl-rules-type (car (car args)))
        (equal? strategy-type (car (cadr args)))
        (let* ((parsed-groups (cdr (car args)))
               (rule-groups (parsed-groups->rule-groups parsed-groups))
               (body (caddr args))
               (do (hashmap-set! rule-groups '^ env))
               (re-arg (cadr args))
               (re-tree (parse-re rule-groups re-arg)))
          (eval-re eval-tree apply-branch re-tree body)))))




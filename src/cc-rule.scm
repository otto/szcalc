;;;; Copyright (C) 2020, 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (functional-rule-lambda) "./functional-rule.scm"

%var cc-rule
%var continue

;; current continuation
(define cc-rule
  (functional-rule-lambda
   args
   (call/cc
    (lambda (k)
      (cons k args)))))

(define (continue T value . tags)
  (let loop ((T T))
    (when (pair? T)
      (if (and (procedure? (car T))
               (and-map (lambda (tag) (member tag (cdr T))) tags))
          ((car T) value)
          (map loop T))))
  #f)

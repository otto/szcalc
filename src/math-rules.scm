;;;; Copyright (C) 2020, 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (convert-number-base) "./euphrates/convert-number-base.scm"
%use (simple-predicate) "./simple-predicate.scm"

%var add-rule
%var mul-rule
%var exp-rule
%var modulo-rule
%var base-rule
%var binor-rule
%var binand-rule
%var binxor-rule
%var bincount-rule
%var binneg-rule
%var binsneg-rule
%var floor-rule
%var ceiling-rule

(define (number-or-false? x)
  (or (not x) (number? x)))

(define-syntax-rule (math-predicate args set-args . bodies)
  (simple-predicate
   args set-args
   (and
    (and-map number-or-false? (list . args))
    (let () . bodies))))

(define (set-or-compare x set-x new-x)
  (if x
      (equal? x new-x)
      (set-x new-x)))

(define (bin-predicate action left-invert right-invert)
  (math-predicate
   (a b R) (set-a set-b set-R)
   (or
    (and a b (set-or-compare R set-R (action a b)))
    (and a R (set-or-compare b set-b (left-invert R a)))
    (and b R (set-or-compare a set-a (right-invert R b))))))

(define (bin-comm-predicate action invert)
  (bin-predicate action invert invert))

(define (one-way-bin-predicate action)
  (math-predicate
   (a b R) (set-a set-b set-R)
   (and a b (set-or-compare R set-R (action a b)))))

(define (one-way-predicate action)
  (math-predicate
   (a R) (set-a set-R)
   (and a (set-or-compare R set-R (action a)))))

(define (safe-div a b)
  (and (not (= 0 b)) (/ a b)))

(define (exp-right-invert a b)
  (and (not (= 0 b))
       (let ((p (/ 1 b)))
         (expt a p))))

(define (logb base exp)
  (/ (log exp) (log base)))

(define (safe-logb b a)
  (and (not (= 0 a))
       (not (= 0 b))
       (not (= 1 a))
       (logb a b)))

(define add-rule
  (bin-comm-predicate + -))

(define mul-rule
  (bin-comm-predicate * safe-div))

(define exp-rule
  (bin-predicate expt safe-logb exp-right-invert))

(define binor-rule
  (one-way-bin-predicate logior))
(define binand-rule
  (one-way-bin-predicate logand))
(define binxor-rule
  (bin-comm-predicate logxor logxor)) ;; TODO: 0 is an edge case?

(define bincount-rule
  (one-way-predicate logcount))
(define binneg-rule
  (one-way-predicate
   (lambda (x)
     (logxor x (- (expt 2 (+ (inexact->exact (floor (logb 2 x))) 1)) 1)))))
(define binsneg-rule
  (one-way-predicate lognot))

(define floor-rule
  (one-way-predicate
   (compose inexact->exact floor)))
(define ceiling-rule
  (one-way-predicate
   (compose inexact->exact ceiling)))

(define modulo-rule
  (one-way-bin-predicate modulo))

(define base-rule
  (one-way-bin-predicate
   (lambda (target-base x)
     (list->string (convert-number-base target-base x)))))


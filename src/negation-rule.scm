;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (virtual-env-eval) "./virtual-env.scm"

%var negation-rule

;; NOTE: only #f returns #t, but many truthy values are possible.
;; This means that this translation is not always correct:
;;  original:
;;     f X -> yes | test X
;;     f X -> no
;;  translated:
;;     f X -> no | $(not (test X))
;;     f X -> yes
(define negation-rule
  (lambda (venv env eval-tree apply-branch args-H key args)
    (if (list? args)
        (and (pair? args)
             (let ((arg (car args)))
               (not (virtual-env-eval
                     venv env
                     eval-tree apply-branch
                     args-H (car arg) (cdr arg)))))
        (not (virtual-env-eval
              venv env
              eval-tree apply-branch
              args-H args '())))))

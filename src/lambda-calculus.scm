;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (simple-predicate) "./simple-predicate.scm"

%var alpha-beta-reduce-rule
%var free-variables-rule

(define (lambda-abstraction? x)
  (and (pair? x)
       (not (null? (cdr x)))
       (eq? ': (cadr x))))

(define (recurse-beta arg body applicant)
  (let loop ((body body))
    (if (list? body)
        (if (and (lambda-abstraction? body)
                 (eq? arg (car body)))
            body
            (map loop body))
        (if (eq? arg body)
            applicant
            body))))

(define (free-variables term)
  (let loop ((term term) (bound '()))
    (cond
     ((lambda-abstraction? term)
      (let ((arg (car term))
            (body (cddr term)))
        (loop body (cons arg bound))))
     ((pair? term)
      (apply append (map (lambda (t) (loop t bound)) term)))
     (else
      (if (member term bound) '() (list term))))))

(define free-variables-rule
  (simple-predicate
   (var body) _
   (member var (free-variables body))))

(define (alpha-beta-reduce-rule env eval-tree apply-branch T)
  (and (pair? T)
       (not (null? (cdr T)))
       (let ((abstraction (car T))
             (applicant (cadr T))
             (rest (cddr T)))
         (and (lambda-abstraction? abstraction)
              (let ((arg (car abstraction))
                    (body (cddr abstraction)))
                (let ((R (recurse-beta arg body applicant)))
                  (if (null? rest) R
                      (if (and (pair? R)
                               (null? (cdr R)))
                          (cons (car R) rest)
                          (cons R rest)))))))))




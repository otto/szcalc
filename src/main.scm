;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (words->string) "./euphrates/words-to-string.scm"
%use (open-file-port) "./euphrates/open-file-port.scm"
%use (read-list) "./euphrates/read-list.scm"
%use (memconst) "./euphrates/memconst.scm"
%use (raisu) "./euphrates/raisu.scm"
%use (with-cli define-cli:show-help) "./euphrates/define-cli.scm"
%use (~a) "./euphrates/tilda-a.scm"
%use (read-tree) "./read-tree.scm"
%use (current-out/p loop-eval default-eval-tree default-apply-branch) "./eval.scm"
%use (let-rule) "./let-rule.scm"
%use (eq-rule) "./eq-rule.scm"
%use (add-rule mul-rule exp-rule modulo-rule base-rule binor-rule binand-rule binxor-rule bincount-rule binneg-rule binsneg-rule floor-rule ceiling-rule)  "./../src/math-rules.scm"
%use (cc-rule continue) "./cc-rule.scm"
%use (strategy-type redsl-rule redsl-read redsl-read-rule redsl-parse redsl-parse-rule) "./redsl.scm"
%use (less-rule) "./less-rule.scm"
%use (list-type) "./list-type.scm"
%use (alpha-beta-reduce-rule free-variables-rule) "./lambda-calculus.scm"
%use (virtual-env/p virtual-env-create-from-alist) "./virtual-env.scm"
%use (ref-rule) "./ref-rule.scm"
%use (atom-rule) "./atom-rule.scm"
%use (negation-rule) "./negation-rule.scm"
%use (fresh-rule) "./fresh-rule.scm"
%use (galphaunique-rule) "./alphaunique.scm"
%use (show-tree) "./show-tree.scm"

%for (COMPILER "guile")
(use-modules (ice-9 pretty-print))
%end

(define (test-out T)
  (when T (show-tree T))
  T)

(define (display-all T)
  (for-each
   (lambda (p)
     (display p) (display " "))
   T))

(define (go-eval show input-tree)
  (parameterize ((current-out/p show)
                 (virtual-env/p
                  (virtual-env-create-from-alist
                   (list
                    (cons '$lt less-rule)
                    (cons '= eq-rule)
                    (cons '$add add-rule)
                    (cons '$mul mul-rule)
                    (cons '$exp exp-rule)
                    (cons '$mod modulo-rule)
                    (cons '$base base-rule)
                    (cons '$binor binor-rule)
                    (cons '$binand binand-rule)
                    (cons '$binxor binxor-rule)
                    (cons '$binneg binneg-rule)
                    (cons '$binsneg binsneg-rule)
                    (cons '$bincount bincount-rule)
                    (cons '$floor floor-rule)
                    (cons '$ceiling ceiling-rule)
                    (cons '$ref ref-rule)
                    (cons '$atom atom-rule)
                    (cons '$FV free-variables-rule)
                    (cons '$not negation-rule)
                    (cons '$fresh fresh-rule)
                    (cons '$galphaunique galphaunique-rule)
                    ))))

    (loop-eval
     (list
      alpha-beta-reduce-rule
      (let-rule '$let)
      (cc-rule '$cc)
      (redsl-rule 'redsl)
      (redsl-read-rule 'redsl-read)
      (redsl-parse-rule 'redsl-parse)
      )
     default-eval-tree default-apply-branch input-tree)))

(define (show-tree T1)
  (newline)
  (cond

%for (COMPILER "guile")
   (#t (pretty-print T1))
%end

   (#t
    (let loop ((T T1))
      (if (list? T)
          (if (null? (cdr T))
              (loop (car T))
              (display-all T))
          (display T)))))
  (newline))

(define (go show input-tree)
  (show-tree (go-eval show input-tree)))

(with-cli
 (START
  START : --help
  /       OPTS* BODY OPTS*
  BODY  : --file <file>
  /       --rules <rules-file> --strategy <strategy> EXPR*
  /       --continue <rules-file> EXPR*
  EXPR : <expr...>

  OPTS  : --no-trace
  /       --trace
  )

 :exclusive (--no-trace --trace)
 :default (--no-trace #t)

 (define show
   (if --no-trace identity test-out))

 (define expr
   (memconst
    (if <expr...>
        (with-input-from-string (words->string (reverse <expr...>)) read-tree)
        (read-tree))))

 (define (go-with-rules show rules strategy)
   (let* ((input-tree
           `(redsl
             ,rules
             ,strategy
             ,(expr))))
     (unless --no-trace
       (show input-tree))
     (go show input-tree)))

 (cond
  (--help
   (define-cli:show-help))
  (--file
   (let* ((fileport (open-file-port <file> "r"))
          (input-tree (read-tree fileport)))
     (go show input-tree)))
  (--continue
   (raisu 'Not-implemented))
  (--rules
   (let ((rules (redsl-read <rules-file>))
         (strategy (cons strategy-type (with-input-from-string <strategy> (lambda _ (read-list))))))
     (go-with-rules show rules strategy)))
  (else
   (raisu 'impossible-arguments))))

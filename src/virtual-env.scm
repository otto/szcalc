;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (hashmap hashmap?) "./euphrates/hashmap.scm"
%use (hashmap-clear! hashmap-set! hashmap-ref alist->hashmap) "./euphrates/ihashmap.scm"
%use (make-box box? box-ref box-set!) "./euphrates/box.scm"
%use (raisu) "./euphrates/raisu.scm"
%use (variable-name?) "./variable-name-p.scm"
%use (replace-match) "./match.scm"

%var virtual-env/p
%var virtual-env-create-from-alist
%var virtual-env-eval
%var args->values

(define (virtual-set-create)
  (make-box (list)))

(define (virtual-set? x)
  (box? x))

(define (virtual-set-add! S value)
  (box-set! S (cons value (box-ref S)))
  #t)

(define (args->values H args)
  (define (get k)
    (if (variable-name? k)
        (hashmap-ref H k #f)
        (replace-match H k)))
  (map get args))

(define (virtual-set-get S args-H args)
  (define argv (args->values args-H args))

  (define (matches? pattern target)
    (let loop ((pattern pattern) (target target))
      (if (null? pattern)
          (null? target)
          (let ((p (car pattern))
                (t (car target)))
            (and (or (not p)
                     (equal? t p))
                 (loop (cdr pattern) (cdr target)))))))

  (or-map
   (lambda (value)
     (and (matches? argv value)
          (begin
            (for-each
             (lambda (ars arg val)
               (unless arg
                 (hashmap-set! args-H ars val)))
             args argv value)
            #t)))
   (box-ref S)))

(define (virtual-set-eval set? S args-H args)
  (if set?
      (virtual-set-add! S (args->values args-H args))
      (virtual-set-get S args-H args)))

(define (virtual-env-create)
  (hashmap))

(define virtual-env/p
  (make-parameter
   (virtual-env-create)))

(define (virtual-env-create-from-alist alist)
  (alist->hashmap alist))

(define (virtual-env-add! venv key value)
  (hashmap-set! venv key value))

(define (virtual-env-key-set? key)
  (string-suffix? "!" (symbol->string key)))

(define (virtual-env-eval venv env eval-tree apply-branch args-H key args)
  (define v0 (hashmap-ref venv key #f))
  (or
   (and v0
        (cond
         ((procedure? v0)
          (v0 venv env eval-tree apply-branch args-H key args))
         ((virtual-set? v0)
          (virtual-set-eval (virtual-env-key-set? key) v0 args-H args))
         (else
          (raisu 'bad-value-in-the-hashtable v0))))
   (and (virtual-env-key-set? key)
        (let ((b (virtual-set-create))
              (argv (args->values args-H args))
              (get-name
               (string->symbol
                (substring
                 (symbol->string key)
                 0 (- (string-length (symbol->string key)) 1)))))
          (virtual-set-add! b argv)
          (hashmap-set! venv key b)
          (hashmap-set! venv get-name b)
          #t))))




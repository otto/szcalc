;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (hashmap hashmap?) "./euphrates/hashmap.scm"
%use (hashmap-ref hashmap-set! hashmap-clear! hashmap-foreach hashmap-copy hashmap->alist alist->hashmap) "./euphrates/ihashmap.scm"
%use (prepare-match replace-match) "./match.scm"
%use (loop-eval) "./eval.scm"
%use (virtual-env/p virtual-env-eval) "./virtual-env.scm"

%var make-match-rule

(define (check-condition venv env eval-tree apply-branch H cond)
  (and (pair? cond)
       (virtual-env-eval venv env eval-tree apply-branch H (car cond) (cdr cond))))

(define make-match-rule
  (let ((global-hash-p (make-parameter (hashmap))))
    (lambda (pattern result conditions)
      (let ((match (prepare-match pattern))
            (venv (virtual-env/p)))
        (lambda (env eval-tree apply-branch T)
          (define global-hash (or (global-hash-p) (hashmap)))
          (hashmap-clear! global-hash)
          (let ((R (match global-hash T)))
            (and (null? R)
                 (or (null? conditions)
                     (parameterize ((global-hash-p #f))
                       (and-map (lambda (T) (check-condition venv env eval-tree apply-branch global-hash T)) conditions)))
                 (replace-match global-hash result))))))))

;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (simple-predicate) "./simple-predicate.scm"

%var less-rule

(define (compare-atoms a b)
  (cond
   ((symbol? a)
    (cond
     ((symbol? b) (compare-atoms (symbol-hash a) (symbol-hash b)))
     (else 'GT)))
   (else
    (cond
     ((symbol? b) 'LT)
     (else
      (cond
       ((< a b) 'LT)
       ((> a b) 'GT)
       (else 'EQ)))))))

(define (compare-lists as bs)
  (let loop ((as as) (bs bs))
    (if (null? as)
        (if (null? bs)
            'EQ
            'LT)
        (if (null? bs)
            'GT
            (let ((cmp (compare-tree (car as) (car bs))))
              (if (eq? 'EQ cmp)
                  (loop (cdr as) (cdr bs))
                  cmp))))))

(define (compare-tree a b)
  (if (pair? a)
      (if (pair? b)
          (compare-lists a b)
          'GT)
      (if (pair? b)
          'LT
          (compare-atoms a b))))

(define (tree-less? a b)
  (eq? 'LT (compare-tree a b)))

(define less-rule
  (simple-predicate
   (a b) _
   (and a b
        (tree-less? a b))))

;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%var current-out/p
%var default-apply-branch
%var default-eval-tree
%var loop-eval
%var topdown-eval-tree

(define current-out/p
  (make-parameter identity))

(define loop-eval
  (case-lambda
   ((env eval-tree apply-branch T0 return)
    (define out (current-out/p))
    (let loop ((T T0) (evaled? #f))
      (let* ((T* (out (eval-tree env eval-tree apply-branch T))))
        (if T* (loop T* #t) (return T evaled?)))))

   ((env eval-tree apply-branch T)
    (define out (current-out/p))
    (let loop ((T T))
      (let* ((T* (out (eval-tree env eval-tree apply-branch T))))
        (if T* (loop T*) T))))))

(define (default-apply-branch env eval-tree apply-branch T)
  (or-map (lambda (f) (f env eval-tree apply-branch T)) env))

(define (default-eval-subtrees env eval-tree apply-branch T)
  (let loop ((T T) (any-evaluated? #f) (ret '()))
    (if (null? T) (and any-evaluated? (reverse ret))
        (let* ((e0 (eval-tree env eval-tree apply-branch (car T)))
               (e (or e0 (car T))))
          (loop (cdr T) (or any-evaluated? e0)
                (if (pair? e)
                    (if (null? e) ret
                        (if (null? (cdr e))
                            (cons (car e) ret)
                            (cons e ret)))
                    (cons e ret)))))))

(define (default-eval-tree env eval-tree apply-branch T)
  (and (pair? T)
       (or (apply-branch env eval-tree apply-branch T)
           (default-eval-subtrees env eval-tree apply-branch T))))

;; More intensive than default-eval-tree,
;; but if apply-branch is default-apply-branch,
;; then this still will not evaluate two overlapping patterns on the same level
(define (topdown-eval-tree env eval-tree apply-branch T)
  (and (pair? T)
       (let ((T* (apply-branch env eval-tree apply-branch T)))
         (if T*
             (or (default-eval-subtrees env eval-tree apply-branch T*) T*)
             (default-eval-subtrees env eval-tree apply-branch T)))))

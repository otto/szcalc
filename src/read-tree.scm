;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

%run guile

%use (read-list) "./euphrates/read-list.scm"
%use (tree-map-leafs) "./euphrates/tree-map-leafs.scm"
%use (string-split/simple) "./euphrates/string-split-simple.scm"
%use (convert-number-base) "./euphrates/convert-number-base.scm"

%var read-tree

(define read-tree
  (case-lambda
   (() (read-tree (current-input-port)))
   ((port)
    (define initial (read-list port))

    (define (read-based-number/string str)
      (define sp (string-split/simple str #\#))
      (and (= (length sp) 2)
           (let* ((digits (car sp))
                  (base0 (cadr sp))
                  (base (string->number base0)))
             (and base
                  (not (string-null? digits))
                  (string->number
                   (convert-number-base base 10 digits))))))

    (define (read-based-number x)
      (cond
       ((symbol? x)
        (or (read-based-number/string (symbol->string x)) x))
       ((string? x)
        (or (read-based-number/string x) x))
       (else x)))

    (tree-map-leafs read-based-number initial))))
